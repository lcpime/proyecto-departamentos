import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutenticacionRoutes } from './modules/autenticacion/routing/autenticacion-routes';
import { AppModules } from './routing/app-module';

const routes: Routes = [

    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/' + AppModules.autenticacion.id + AutenticacionRoutes.login.uri
    },
    {
        path: AppModules.autenticacion.id,
        loadChildren: () => import('./modules/autenticacion/autenticacion.module')
        .then(m => m.AutenticacionModule)
    },
    {
        path: AppModules.paginaPrincipal.id,
        loadChildren: () => import('./modules/dashboard/dashboard.module')
        .then(m => m.DashboardModule)
    },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: '/' + AppModules.autenticacion.id + AutenticacionRoutes.login.uri
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
