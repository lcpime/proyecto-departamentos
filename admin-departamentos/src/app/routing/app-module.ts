import { ModuleContext } from "../shared/model/routing/module-context"


export class AppModules{


    static readonly autenticacion: ModuleContext = {
        id: 'autenticacion',
        description: 'Modulo para la autenticacion'
    }

    static readonly paginaPrincipal: ModuleContext = {
        id: 'gestion-departamentos',
        description: 'Modulo para la pagina principal del dashboard'
    }
}
