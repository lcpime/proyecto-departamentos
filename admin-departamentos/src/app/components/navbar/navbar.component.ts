import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RouterService } from 'src/app/shared/services/router.service';
import { UserService } from 'src/app/shared/services/user.service';
@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
    conectado: boolean;

    constructor(private userService: UserService,
        private router:RouterService) {
        this.conectado = false;
    }

    ngOnInit(): void {
        this.conectado = this.userService.sesion;
    }

    irHome(){
        return this.router.cuaPrincipal();
    }


}
