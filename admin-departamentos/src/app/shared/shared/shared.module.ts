import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {MatIconModule} from "@angular/material/icon";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSidenavModule} from "@angular/material/sidenav";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {MatTableModule} from "@angular/material/table";
import {MatTooltipModule} from "@angular/material/tooltip";
import { NgxCaptchaModule } from 'ngx-captcha';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { CardStatsComponent } from 'src/app/components/cards/card-stats/card-stats.component';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';



@NgModule({
    declarations: [CardStatsComponent],
    imports: [
        CommonModule
    ],
    exports: [
        CardStatsComponent,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        NgxCaptchaModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        HttpClientModule,
        MatIconModule,
        MatToolbarModule,
        MatSidenavModule,
        MatTableModule,
        MatPaginatorModule,
        MatTooltipModule,
        MatCardModule,
        MatDatepickerModule,
        MatNativeDateModule,
        
    ]
})
export class SharedModule {
}
