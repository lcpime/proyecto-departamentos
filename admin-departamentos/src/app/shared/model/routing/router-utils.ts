import { ModuleContext } from "./module-context";
import { RouteContext } from "./route-context";

export class RouterUtils {
    static getRoute(module: ModuleContext, route?: RouteContext, suffix: string = ''): string {
        return '/' + module.id + (route?.uri || '') + suffix;
    }
}

