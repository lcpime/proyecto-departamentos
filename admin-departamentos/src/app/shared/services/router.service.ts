import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { AutenticacionRoutes } from "src/app/modules/autenticacion/routing/autenticacion-routes";
import { DashboardRoutingModule } from "src/app/modules/dashboard/dashboard-routing.module";
import { PrincipalRoutes } from "src/app/modules/dashboard/pantalla-principal/routing/principal-routes";
import { DashboardModules } from "src/app/modules/dashboard/routing/dashboard-modules";
import { AppModules } from "src/app/routing/app-module";
import { RouterUtils } from "../model/routing/router-utils";

@Injectable({
    providedIn: "root",
})
export class RouterService {
    private pila: Array<string> = [];

    constructor(private router: Router) {}

    //Modulo de autenticacion
    public cuaLogin(): void {
        this.ir(AppModules.autenticacion.id + AutenticacionRoutes.login.uri); //autenticacion/login
    }

    //Modulo de la pagina principal de el dashboard
    public cuaPrincipal(): void {
        this.ir(
            RouterUtils.getRoute(
                AppModules.paginaPrincipal,
                undefined,
                RouterUtils.getRoute(
                    DashboardModules.Principal,
                    PrincipalRoutes.inicio
                )
            )
        );
    }

    //Modulo de la pagina recibos de luz de el dashboard
    public cuaElectricidad(): void {
        this.ir(
            RouterUtils.getRoute(
                AppModules.paginaPrincipal,
                undefined,
                RouterUtils.getRoute(
                    DashboardModules.Principal,
                    PrincipalRoutes.recibosLuz
                )
            )
        );
    }

    //Modulo para el monitoreo de inquilinos
    public cuaInquilinos(): void {
        this.ir(
            RouterUtils.getRoute(
                AppModules.paginaPrincipal,
                undefined,
                RouterUtils.getRoute(
                    DashboardModules.Principal,
                    PrincipalRoutes.monitoreoInquilinos
                )
            )
        );
    }

    //Modulo para el monitoreo de departamentos
    public cuaDepartamentos(): void {
        this.ir(
            RouterUtils.getRoute(
                AppModules.paginaPrincipal,
                undefined,
                RouterUtils.getRoute(
                    DashboardModules.Principal,
                    PrincipalRoutes.gestionDepartamentos
                )
            )
        );
    }

        //Modulo para el monitoreo de rentas
        public cuaRentas(): void {
            this.ir(
                RouterUtils.getRoute(
                    AppModules.paginaPrincipal,
                    undefined,
                    RouterUtils.getRoute(
                        DashboardModules.Principal,
                        PrincipalRoutes.gestionRentas
                    )
                )
            );
        }

    //Metodos iniciales
    private ir(uri: string): void {
        this.pila.push(this.router.url);
        this.router.navigate(["/" + uri]);
    }

    private atras(): void {
        if (this.pila.length > 0) {
            this.router.navigate([this.pila.pop()]);
        }
    }
}
