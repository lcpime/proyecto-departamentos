import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class UserService {
    sesion: boolean;

    private $user: BehaviorSubject<UserData | null> =
        new BehaviorSubject<UserData | null>(null);
    private user$: Observable<UserData | null> = this.$user.asObservable();
    constructor() {
        this.sesion = true;
    }

    private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
        false,
    );

    get isLoggedIn() {
        return this.loggedIn.asObservable();
    }

    getUserData(): UserData | null {
        return this.$user.value;
    }

    userDataChanges(): Observable<UserData | null> {
        return this.user$;
    }


    login(credenciales: Credenciales): boolean {
        if (
            credenciales.email === "lcpime@gmail.com" &&
            credenciales.password === "12345678"
        ) {
            this.loggedIn.next(true);
            this.$user.next({
                nombre: "Luis",
                apellido: "Pimentel",
                rol: "RO",
            });
            
            return true;
        } else {
            return false;
        }
    }
}

export interface UserData {
    nombre: string;
    apellido: string;
    rol: string;
}

export interface Credenciales {
    email: string;
    password: string;
}
