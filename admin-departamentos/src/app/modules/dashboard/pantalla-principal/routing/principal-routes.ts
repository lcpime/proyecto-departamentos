
import { RouteContext } from "src/app/shared/model/routing/route-context";


export class PrincipalRoutes {
    static readonly inicio:RouteContext = {
        uri: '/inicio',
        ucName: 'DA-CU1'
    };

    static readonly recibosLuz:RouteContext = {
        uri: '/electricidad',
        ucName: 'DA-CU1'
    };

    static readonly monitoreoInquilinos:RouteContext = {
        uri: '/inquilinos',
        ucName: 'DA-CU1'
    };

    static readonly gestionDepartamentos:RouteContext = {
        uri: '/departamentos',
        ucName: 'DA-CU1'
    };

    static readonly gestionRentas:RouteContext = {
        uri: '/rentas',
        ucName: 'DA-CU1'
    };

}
