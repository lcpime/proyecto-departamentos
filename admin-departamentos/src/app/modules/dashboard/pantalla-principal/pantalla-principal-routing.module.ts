import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MonitoreoInquilinosComponent } from './components/monitoreo-inquilinos/monitoreo-inquilinos.component';
import { PrincipalComponent } from './components/principal/principal.component';
import { RecibosLuzComponent } from './components/recibos-luz/recibos-luz.component';
import { PrincipalRoutes } from './routing/principal-routes';
import { MonitoreoDepartamentosComponent } from './components/monitoreo-departamentos/monitoreo-departamentos.component';
import { MonitoreoRentasComponent } from './components/monitoreo-rentas/monitoreo-rentas.component';

const routes: Routes = [
    {
        path: PrincipalRoutes.inicio.uri.slice(1),
        component: PrincipalComponent,
    },
    {
        path: PrincipalRoutes.recibosLuz.uri.slice(1),
        component: RecibosLuzComponent,
    },
    {
        path: PrincipalRoutes.monitoreoInquilinos.uri.slice(1),
        component: MonitoreoInquilinosComponent,
    },
    {
        path: PrincipalRoutes.gestionDepartamentos.uri.slice(1),
        component: MonitoreoDepartamentosComponent,
    },
    {
        path: PrincipalRoutes.gestionRentas.uri.slice(1),
        component: MonitoreoRentasComponent,
    },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PantallaPrincipalRoutingModule { }
