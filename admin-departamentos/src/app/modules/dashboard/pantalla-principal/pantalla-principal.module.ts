import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PantallaPrincipalRoutingModule } from './pantalla-principal-routing.module';
import { PrincipalComponent } from './components/principal/principal.component';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { RecibosLuzComponent } from './components/recibos-luz/recibos-luz.component';
import { MonitoreoInquilinosComponent } from './components/monitoreo-inquilinos/monitoreo-inquilinos.component';
import { MonitoreoDepartamentosComponent } from './components/monitoreo-departamentos/monitoreo-departamentos.component';
import { MonitoreoRentasComponent } from './components/monitoreo-rentas/monitoreo-rentas.component';


@NgModule({
  declarations: [
    PrincipalComponent,
    RecibosLuzComponent,
    MonitoreoInquilinosComponent,
    MonitoreoDepartamentosComponent,
    MonitoreoRentasComponent
  ],
  imports: [
    CommonModule,
    PantallaPrincipalRoutingModule,
    SharedModule
  ]
})
export class PantallaPrincipalModule { }
