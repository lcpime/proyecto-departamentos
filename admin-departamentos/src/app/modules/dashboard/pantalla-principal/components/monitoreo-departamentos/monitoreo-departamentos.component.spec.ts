import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoreoDepartamentosComponent } from './monitoreo-departamentos.component';

describe('MonitoreoDepartamentosComponent', () => {
  let component: MonitoreoDepartamentosComponent;
  let fixture: ComponentFixture<MonitoreoDepartamentosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonitoreoDepartamentosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoreoDepartamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
