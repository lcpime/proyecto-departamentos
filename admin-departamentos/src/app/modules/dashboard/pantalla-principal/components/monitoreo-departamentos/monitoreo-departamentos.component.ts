import { Component, OnInit } from '@angular/core';
import { RouterService } from '../../../../../shared/services/router.service';

@Component({
  selector: 'app-monitoreo-departamentos',
  templateUrl: './monitoreo-departamentos.component.html',
  styleUrls: ['./monitoreo-departamentos.component.scss']
})
export class MonitoreoDepartamentosComponent implements OnInit {

  constructor(private router:RouterService) { }

  ngOnInit(): void {
  }


  irHome(){
      return this.router.cuaPrincipal();
  }
}
