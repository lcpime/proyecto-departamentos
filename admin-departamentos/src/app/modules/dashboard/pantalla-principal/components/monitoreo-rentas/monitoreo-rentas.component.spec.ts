import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoreoRentasComponent } from './monitoreo-rentas.component';

describe('MonitoreoRentasComponent', () => {
  let component: MonitoreoRentasComponent;
  let fixture: ComponentFixture<MonitoreoRentasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonitoreoRentasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoreoRentasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
