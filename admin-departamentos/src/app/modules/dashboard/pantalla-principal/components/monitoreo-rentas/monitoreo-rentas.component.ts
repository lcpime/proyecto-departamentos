import { Component, OnInit } from '@angular/core';
import { RouterService } from 'src/app/shared/services/router.service';

@Component({
  selector: 'app-monitoreo-rentas',
  templateUrl: './monitoreo-rentas.component.html',
  styleUrls: ['./monitoreo-rentas.component.scss']
})
export class MonitoreoRentasComponent implements OnInit {

    constructor(private router:RouterService) { }

    ngOnInit(): void {
    }


    irHome(){
        return this.router.cuaPrincipal();
    }

}
