import { Component, OnInit } from '@angular/core';
import { RouterService } from '../../../../../shared/services/router.service';

@Component({
  selector: 'app-recibos-luz',
  templateUrl: './recibos-luz.component.html',
  styleUrls: ['./recibos-luz.component.scss']
})
export class RecibosLuzComponent implements OnInit {

  constructor(private router:RouterService) { }

  ngOnInit(): void {
  }

  irHome(){
    return this.router.cuaPrincipal();
}

}
