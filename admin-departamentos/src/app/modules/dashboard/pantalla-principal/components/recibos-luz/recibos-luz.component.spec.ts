import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibosLuzComponent } from './recibos-luz.component';

describe('RecibosLuzComponent', () => {
  let component: RecibosLuzComponent;
  let fixture: ComponentFixture<RecibosLuzComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecibosLuzComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibosLuzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
