import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { RouterService } from '../../../../../shared/services/router.service';



@Component({
    selector: 'app-monitoreo-inquilinos',
    templateUrl: './monitoreo-inquilinos.component.html',
    styleUrls: ['./monitoreo-inquilinos.component.scss'],
})
export class MonitoreoInquilinosComponent implements OnInit,AfterViewInit{
    displayedColumns: string[] = ['nombre', 'edad', 'fechaRegistro', 'deposito', 'documentacion', 'estatusContrato', 'acciones'];
    dataSource: MatTableDataSource<PeriodicElement>;

    @ViewChild(MatPaginator) paginator!: MatPaginator;
    //@ViewChild(MatSort) sort: MatSort;

    constructor(private router: RouterService) {
        this.dataSource = new MatTableDataSource<PeriodicElement>();
    }

    ngOnInit(): void {
        this.loadData();
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
      }


      public loadData(){
          this.dataSource.data = [
            {
                nombre: 'Luis Carlos Pimentel Escobedo',
                edad: 26,
                fechaRegistro: '11/07/2022',
                deposito: '1900',
                documentacion: true,
                estatusContrato: 'Activo'
            },
            {
                nombre: 'Luis Carlos Pimentel Escobedo',
                edad: 26,
                fechaRegistro: '11/07/2022',
                deposito: '1900',
                documentacion: true,
                estatusContrato: 'Activo'
            },
            {
                nombre: 'Luis Carlos Pimentel Escobedo',
                edad: 26,
                fechaRegistro: '11/07/2022',
                deposito: '1900',
                documentacion: true,
                estatusContrato: 'Activo'
            },
            {
                nombre: 'Luis Carlos Pimentel Escobedo',
                edad: 26,
                fechaRegistro: '11/07/2022',
                deposito: '1900',
                documentacion: true,
                estatusContrato: 'Activo'
            },
            {
                nombre: 'Luis Carlos Pimentel Escobedo',
                edad: 26,
                fechaRegistro: '11/07/2022',
                deposito: '1900',
                documentacion: true,
                estatusContrato: 'Activo'
            },
            {
                nombre: 'Luis Carlos Pimentel Escobedo',
                edad: 26,
                fechaRegistro: '11/07/2022',
                deposito: '1900',
                documentacion: true,
                estatusContrato: 'Activo'
            },
            {
                nombre: 'Luis Carlos Pimentel Escobedo',
                edad: 26,
                fechaRegistro: '11/07/2022',
                deposito: '1900',
                documentacion: true,
                estatusContrato: 'Activo'
            },
            {
                nombre: 'Luis Carlos Pimentel Escobedo',
                edad: 26,
                fechaRegistro: '11/07/2022',
                deposito: '1900',
                documentacion: true,
                estatusContrato: 'Activo'
            },
            {
                nombre: 'Luis Carlos Pimentel Escobedo',
                edad: 26,
                fechaRegistro: '11/07/2022',
                deposito: '1900',
                documentacion: true,
                estatusContrato: 'Activo'
            },
            {
                nombre: 'Luis Carlos Pimentel Escobedo',
                edad: 26,
                fechaRegistro: '11/07/2022',
                deposito: '1900',
                documentacion: true,
                estatusContrato: 'Activo'
            },
        ].map(result => {
            const inquilinos = {
                ...result,
                documentacion: result.documentacion ? 'Completa' : 'Pendiente',
            };
            return inquilinos;
        });
      }


    irHome() {
        return this.router.cuaPrincipal();
    }
}



export interface PeriodicElement {
    nombre: string;
    edad: number;
    fechaRegistro: string;
    deposito: string;
    documentacion: string;
    estatusContrato: string;
}
