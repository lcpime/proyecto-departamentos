import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoreoInquilinosComponent } from './monitoreo-inquilinos.component';

describe('MonitoreoInquilinosComponent', () => {
  let component: MonitoreoInquilinosComponent;
  let fixture: ComponentFixture<MonitoreoInquilinosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonitoreoInquilinosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoreoInquilinosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
