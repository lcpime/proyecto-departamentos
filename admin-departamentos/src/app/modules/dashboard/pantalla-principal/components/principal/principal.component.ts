import { Component, OnInit } from '@angular/core';
import { RouterService } from '../../../../../shared/services/router.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {

  constructor(private router:RouterService) { }

  ngOnInit(): void {
  }

  irElectricidad(){
    return this.router.cuaElectricidad();
  }

  irInquilinos(){
    this.router.cuaInquilinos();
  }

  irDepartamentos(){
      this.router.cuaDepartamentos();
  }

  irRentas(){
    this.router.cuaRentas();
}

}
