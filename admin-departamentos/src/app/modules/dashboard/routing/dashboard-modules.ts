import { ModuleContext } from "src/app/shared/model/routing/module-context";


export class DashboardModules{
    static readonly Principal: ModuleContext = {
        id: 'dashboard',
        description: 'Modulo de pantalla principal de dashboard'
      };

}
