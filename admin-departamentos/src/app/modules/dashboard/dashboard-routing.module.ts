import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardModules } from './routing/dashboard-modules';

const routes: Routes = [
    {
        path: DashboardModules.Principal.id,
        loadChildren: () => import('./pantalla-principal/pantalla-principal.module')
        .then((m) => m.PantallaPrincipalModule)
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
