import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioSesionComponent } from './components/inicio-sesion/inicio-sesion.component';
import { AutenticacionRoutes } from './routing/autenticacion-routes';

const routes: Routes = [
    {
        path: AutenticacionRoutes.login.uri.slice(1),
        component: InicioSesionComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutenticacionRoutingModule { }
