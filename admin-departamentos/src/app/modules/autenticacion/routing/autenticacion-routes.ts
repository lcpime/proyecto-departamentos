import { RouteContext } from "src/app/shared/model/routing/route-context";


export class AutenticacionRoutes{
    static readonly login:RouteContext = {
        uri: '/login',
        ucName: 'CU01-DA'
    };
}
