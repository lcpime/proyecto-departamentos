import { Component, OnInit } from '@angular/core';
import {
    AbstractControl,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { ReCaptchaV3Service } from 'ngx-captcha';
import { RouterService } from 'src/app/shared/services/router.service';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
    selector: 'app-inicio-sesion',
    templateUrl: './inicio-sesion.component.html',
    styleUrls: ['./inicio-sesion.component.scss'],
    providers: [RouterService],
})
export class InicioSesionComponent implements OnInit {
    form: FormGroup;

    constructor(private router: RouterService,
        private userService:UserService) {
        this.form = new FormGroup({
            email: new FormControl('ceneval@gmail.com', [Validators.email, Validators.required]),
            password: new FormControl('12345678', [Validators.required]),
        });
    }

    get email(): AbstractControl | null {
        return this.form.get('email');
    }

    get password(): AbstractControl | null {
        return this.form.get('password');
    }

    ngOnInit(): void {}

    login(): void{
        this.form.markAllAsTouched();
        if(this.form.valid){
            const valorFormulario = this.form.getRawValue();
            this.userService.login(this.form.value);
            this.router.cuaPrincipal();
        }

    }
    }



